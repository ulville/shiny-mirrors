use chrono::{DateTime, Local};
use serde::{Deserialize, Deserializer, Serialize};
use smallvec::SmallVec;
use std::{
	ops::{Deref, DerefMut},
	time::Duration,
};
use url::Url;

use crate::{geo::Country, mirror::Status, USER_AGENT};

use super::{Protocol, Result};

pub const DISTRO_NAME: &str = "Arch";

pub const REPO_ARCH: [&str; 3] = ["$repo", "os", "$arch"];
pub const SMALL_TEST_FILE_DIR: [&str; 2] = ["core", "os"];
pub const BIG_TEST_FILE_DIR: [&str; 2] = ["community", "os"];
pub const MIRRORLIST_URL: &str = "https://archlinux.org/mirrors/status/json/";
pub const MIRROR_LIST_FILE: &str = "/etc/pacman.d/mirrorlist";

// 6.10.2021 Around 176KB
pub const SMALL_TEST_FILE: &str = "core.db.tar.gz";

// 6.10.2021 Around 7MB
// Uses the BIG_TEST_FILE_DIR
pub const MEDIUM_TEST_FILE: &str = "community.db.tar.gz";

// 6.10.2021 Around 31MB
pub const BIG_TEST_FILE: &str = "community.files.tar.gz";

/// List of [mirrors][Mirror]
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct MirrorList {
	urls: Vec<Mirror>,
}

impl MirrorList {
	/// Download the status of mirror list
	pub fn download_mirrors() -> Result<Self> {
		let v: MirrorList = ureq::AgentBuilder::new()
			.user_agent(USER_AGENT)
			.build()
			.get(MIRRORLIST_URL)
			.call()?
			.into_json()?;
		let mut reference_mirrorlist = v.urls.clone();
		// Find all same domains and get all protocols into one entry
		let mut mirrorlist = Self { urls: Vec::new() };
		for mut mirror in v.urls.into_iter() {
			if reference_mirrorlist.iter().any(|x| x.url.domain() == mirror.url.domain()) {
				let protocols: SmallVec<[Protocol; 5]> = reference_mirrorlist
					.iter()
					.take_while(|w| w.url.domain() == mirror.url.domain())
					.map(|x| x.protocols[0])
					.collect();
				reference_mirrorlist.retain(|x| x.url.domain() != mirror.url.domain());
				let _ = mirror.url.set_scheme(&protocols[0].to_string());
				mirrorlist.push(Mirror { protocols, ..mirror })
			}
		}
		Ok(mirrorlist)
	}
}

impl Deref for MirrorList {
	type Target = Vec<Mirror>;

	fn deref(&self) -> &Vec<Mirror> {
		&self.urls
	}
}

impl DerefMut for MirrorList {
	fn deref_mut(&mut self) -> &mut Vec<Mirror> {
		&mut self.urls
	}
}

/// Mirror entry in [Mirror List][MirrorList]
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Mirror {
	pub url: Url,
	pub country: Country,
	/// None == Not measured or failed to connect or such
	#[serde(skip)]
	pub time: Option<Duration>,
	/// None == Unknown | Couldn't parse
	#[serde(deserialize_with = "parse_last_sync")]
	pub last_sync: Option<Duration>,
	#[serde(deserialize_with = "parse_protocol")]
	#[serde(rename = "protocol")]
	pub protocols: SmallVec<[Protocol; 5]>,
	pub completion_pct: f64,
	pub delay: Option<u32>,
	pub ipv6: bool,
	pub ipv4: bool,
	pub active: bool,
}

impl Mirror {
	pub fn get_status(&self) -> Status {
		if self.completion_pct == 1f64 && self.last_sync.map_or(false, |sync| sync <= Duration::from_secs(3_600)) {
			Status::Good
		} else {
			Status::Bad
		}
	}
}

fn parse_protocol<'de, D>(deserializer: D) -> anyhow::Result<SmallVec<[Protocol; 5]>, D::Error>
where
	D: Deserializer<'de>,
{
	let item: Protocol = Deserialize::deserialize(deserializer)?;

	let mut s = SmallVec::new();
	s.push(item);

	Ok(s)
}

fn parse_last_sync<'de, D>(deserializer: D) -> anyhow::Result<Option<Duration>, D::Error>
where
	D: Deserializer<'de>,
{
	let datetime: DateTime<Local> = match Deserialize::deserialize(deserializer) {
		Ok(v) => v,
		Err(_) => return Ok(None),
	};

	let dur = Local::now().signed_duration_since(datetime).to_std().ok();

	Ok(dur)
}
