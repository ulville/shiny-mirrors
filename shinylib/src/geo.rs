use std::str::FromStr;

use anyhow::anyhow;
use serde::{self, Deserialize, Serialize};
use strum::{Display, EnumString, EnumVariantNames};

use Country::*;

use super::{MultiParse, Result, USER_AGENT};

#[allow(non_camel_case_types)]
#[derive(Copy, Clone, Debug, Display, EnumVariantNames, EnumString, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[strum(serialize_all = "title_case")]
#[strum(ascii_case_insensitive)]
#[non_exhaustive]
pub enum Country {
	#[serde(alias = "", alias = "Worldwide")]
	Global,
	// Africa
	Algeria,
	Angola,
	Benin,
	Botswana,
	#[serde(alias = "Burkina Faso")]
	Burkina_Faso,
	Burundi,
	Cameroon,
	#[serde(alias = "Cape Verde")]
	Cape_Verde,
	#[serde(alias = "Central African Republic")]
	Central_African_Republic,
	Chad,
	Comoros,
	#[serde(alias = "Republic of the Congo")]
	Republic_of_the_Congo,
	#[serde(alias = "Democratic Republic of the Congo")]
	Democratic_Republic_of_the_Congo,
	#[serde(alias = "Ivory Coast")]
	Ivory_Coast,
	Djibouti,
	#[serde(alias = "Equatorial Guinea")]
	Equatorial_Guinea,
	Egypt,
	Eritrea,
	Ethiopia,
	Gabon,
	Gambia,
	Ghana,
	Guinea,
	#[serde(alias = "Guinea Bissau")]
	Guinea_Bissau,
	Kenya,
	Lesotho,
	Liberia,
	Libya,
	Madagascar,
	Malawi,
	Mali,
	Mauritania,
	Mauritius,
	Morocco,
	Mayotte,
	Mozambique,
	Namibia,
	Niger,
	Nigeria,
	Rwanda,
	#[serde(alias = "Réunion")]
	Reunion,
	#[serde(alias = "Sao Tome and Principe")]
	Sao_Tome_and_Principe,
	Senegal,
	Seychelles,
	#[serde(alias = "Sierra Leone")]
	Sierra_Leone,
	Somalia,
	#[serde(alias = "South Africa")]
	South_Africa,
	#[serde(alias = "South Sudan")]
	South_Sudan,
	Sudan,
	Eswatini,
	Tanzania,
	Togo,
	Tunisia,
	Uganda,
	#[serde(alias = "Western Sahara")]
	Western_Sahara,
	Zambia,
	Zimbabwe,
	// Asia
	Afghanistan,
	Armenia,
	Azerbaijan,
	Bahrain,
	Bangladesh,
	Bhutan,
	Brunei,
	Cambodia,
	China,
	#[serde(alias = "East Timor")]
	East_Timor,
	India,
	Indonesia,
	Iran,
	Iraq,
	Israel,
	Japan,
	Jordan,
	Kazakhstan,
	Kuwait,
	Kyrgyzstan,
	Laos,
	Lebanon,
	Macau,
	Malaysia,
	Maldives,
	Mongolia,
	Myanmar,
	Nepal,
	#[serde(alias = "North Korea")]
	North_Korea,
	Oman,
	Pakistan,
	Palestine,
	Philippines,
	Qatar,
	#[serde(alias = "Saudi Arabia")]
	Saudi_Arabia,
	Singapore,
	#[serde(alias = "South Korea")]
	South_Korea,
	#[serde(alias = "Sri Lanka")]
	Sri_Lanka,
	Syria,
	Tajikistan,
	Thailand,
	Turkey,
	Turkmenistan,
	Taiwan,
	#[serde(alias = "United Arab Emirates")]
	United_Arab_Emirates,
	Uzbekistan,
	Vietnam,
	Yemen,
	#[serde(alias = "Hong Kong")]
	Hong_Kong,
	Georgia,
	// Europe
	Albania,
	Andorra,
	Austria,
	Belarus,
	Belgium,
	#[serde(alias = "Bosnia and Herzegovina")]
	Bosnia_and_Herzegovina,
	Bulgaria,
	Croatia,
	Cyprus,
	#[serde(alias = "Czech")]
	Czechia,
	Denmark,
	Estonia,
	Finland,
	Germany,
	Greece,
	Hungary,
	Iceland,
	Ireland,
	Italy,
	Kosovo,
	Latvia,
	Liechtenstein,
	Lithuania,
	Luxembourg,
	#[serde(alias = "North Macedonia")]
	North_Macedonia,
	Malta,
	Moldova,
	Monaco,
	Montenegro,
	Netherlands,
	Norway,
	Poland,
	Portugal,
	Romania,
	Russia,
	#[serde(alias = "San Marino")]
	San_Marino,
	Serbia,
	Slovakia,
	Slovenia,
	France,
	Spain,
	Sweden,
	Switzerland,
	Ukraine,
	#[serde(alias = "United Kingdom")]
	United_Kingdom,
	#[serde(alias = "Vatican City")]
	Vatican_City,
	#[serde(alias = "Faroe Islands")]
	Faroe_Islands,
	#[serde(alias = "Isle of Man")]
	Isle_of_Man,
	Gibraltar,
	// North America
	Anguilla,
	#[serde(alias = "Antigua and Barbuda")]
	Antigua_and_Barbuda,
	Bahamas,
	Belize,
	Bermuda,
	Barbados,
	#[serde(alias = "British Virgin Islands")]
	British_Virgin_Islands,
	#[serde(alias = "Cayman Islands")]
	Cayman_Islands,
	Canada,
	#[serde(alias = "Costa Rica")]
	Costa_Rica,
	Cuba,
	Dominica,
	#[serde(alias = "Dominican Republic")]
	Dominican_Republic,
	#[serde(alias = "El Salvador")]
	El_Salvador,
	Grenada,
	Greenland,
	Guatemala,
	Guadeloupe,
	Haiti,
	Honduras,
	Jamaica,
	Martinique,
	Mexico,
	Montserrat,
	Nicaragua,
	Panama,
	#[serde(alias = "Puerto Rico")]
	Puerto_Rico,
	#[serde(alias = "Saint Barthelemy")]
	Saint_Barthelemy,
	#[serde(alias = "Saint Kitts and Nevis")]
	Saint_Kitts_and_Nevis,
	#[serde(alias = "Saint Lucia")]
	Saint_Lucia,
	#[serde(alias = "Sint Maarten")]
	Sint_Maarten,
	#[serde(alias = "Saint Martin")]
	Saint_Martin,
	#[serde(alias = "Saint Pierre and Miquelon")]
	Saint_Pierre_and_Miquelon,
	#[serde(alias = "Saint Vincent and the Grenadines")]
	Saint_Vincent_and_the_Grenadines,
	#[serde(alias = "Turks and Caicos Islands")]
	Turks_and_Caicos_Islands,
	#[serde(alias = "United States")]
	United_States,
	#[serde(alias = "United States Virgin Islands")]
	US_Virgin_Islands,
	// South America
	Argentina,
	Bolivia,
	Brazil,
	Chile,
	Colombia,
	Ecuador,
	#[serde(alias = "Falkland Islands")]
	Falkland_Islands,
	#[serde(alias = "French_Guiana")]
	French_Guiana,
	Guyana,
	Peru,
	Paraguay,
	#[serde(alias = "Trinidad and Tobago")]
	Trinidad_and_Tobago,
	Uruguay,
	Suriname,
	Venezuela,
	Curacao,
	Aruba,
	// Oceania
	Australia,
	Fiji,
	#[serde(alias = "New Zealand")]
	New_Zealand,
	Micronesia,
	Kiribati,
	#[serde(alias = "Marshall Islands")]
	Marshall_Islands,
	Nauru,
	Palau,
	#[serde(alias = "Papua New Guinea")]
	Papua_New_Guinea,
	Samoa,
	#[serde(alias = "Solomon Islands")]
	Solomon_Islands,
	Tonga,
	Tuvalu,
	Vanuatu,
	#[serde(alias = "Timor Leste")]
	Timor_Leste,
	#[serde(alias = "New Caledonia")]
	New_Caledonia,
	#[serde(alias = "French Polynesia")]
	French_Polynesia,
	#[serde(alias = "Cook Islands")]
	Cook_Islands,
	#[serde(alias = "Wallis and Futuna")]
	Wallis_and_Futuna,
	Niue,
	Tokelau,
	#[serde(alias = "Northern Mariana Islands")]
	Northern_Mariana_Islands,
	Guam,
	#[serde(other)]
	Unknown,
}

impl MultiParse<Self> for Country {}

#[allow(non_camel_case_types)]
#[derive(Clone, Copy, Debug, Display, EnumVariantNames, EnumString, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[strum(serialize_all = "title_case")]
#[strum(ascii_case_insensitive)]
#[non_exhaustive]
pub enum Continent {
	Europe,
	Asia,
	Oceania,
	Africa,
	North_America,
	South_America,
}

pub const EUROPE: [Country; 50] = [
	Global,
	Albania,
	Andorra,
	Austria,
	Belarus,
	Belgium,
	Bosnia_and_Herzegovina,
	Bulgaria,
	Croatia,
	Cyprus,
	Czechia,
	Denmark,
	Estonia,
	Finland,
	Germany,
	Greece,
	Hungary,
	Iceland,
	Ireland,
	Italy,
	Kosovo,
	Latvia,
	Liechtenstein,
	Lithuania,
	Luxembourg,
	North_Macedonia,
	Malta,
	Moldova,
	Monaco,
	Montenegro,
	Netherlands,
	Norway,
	Poland,
	Portugal,
	Romania,
	Russia,
	San_Marino,
	Serbia,
	Slovakia,
	Slovenia,
	France,
	Spain,
	Sweden,
	Switzerland,
	Ukraine,
	United_Kingdom,
	Vatican_City,
	Faroe_Islands,
	Isle_of_Man,
	Gibraltar,
];
pub const AFRICA: [Country; 58] = [
	Global,
	Algeria,
	Angola,
	Benin,
	Botswana,
	Burkina_Faso,
	Burundi,
	Cameroon,
	Cape_Verde,
	Central_African_Republic,
	Chad,
	Comoros,
	Republic_of_the_Congo,
	Democratic_Republic_of_the_Congo,
	Ivory_Coast,
	Djibouti,
	Equatorial_Guinea,
	Egypt,
	Eritrea,
	Ethiopia,
	Gabon,
	Gambia,
	Ghana,
	Guinea,
	Guinea_Bissau,
	Kenya,
	Lesotho,
	Liberia,
	Libya,
	Madagascar,
	Malawi,
	Mali,
	Mauritania,
	Mauritius,
	Mayotte,
	Morocco,
	Mozambique,
	Namibia,
	Niger,
	Nigeria,
	Rwanda,
	Reunion,
	Sao_Tome_and_Principe,
	Senegal,
	Seychelles,
	Sierra_Leone,
	Somalia,
	South_Africa,
	South_Sudan,
	Sudan,
	Eswatini,
	Tanzania,
	Togo,
	Tunisia,
	Uganda,
	Western_Sahara,
	Zambia,
	Zimbabwe,
];
pub const ASIA: [Country; 51] = [
	Global,
	Afghanistan,
	Armenia,
	Azerbaijan,
	Bahrain,
	Bangladesh,
	Bhutan,
	Brunei,
	Cambodia,
	China,
	East_Timor,
	India,
	Indonesia,
	Iran,
	Iraq,
	Israel,
	Japan,
	Jordan,
	Kazakhstan,
	Kuwait,
	Kyrgyzstan,
	Laos,
	Lebanon,
	Macau,
	Malaysia,
	Maldives,
	Mongolia,
	Myanmar,
	Nepal,
	North_Korea,
	Oman,
	Pakistan,
	Palestine,
	Philippines,
	Qatar,
	Saudi_Arabia,
	Singapore,
	South_Korea,
	Sri_Lanka,
	Syria,
	Tajikistan,
	Thailand,
	Turkey,
	Turkmenistan,
	Taiwan,
	United_Arab_Emirates,
	Uzbekistan,
	Vietnam,
	Yemen,
	Hong_Kong,
	Georgia,
];
pub const OCEANIA: [Country; 24] = [
	Global,
	Australia,
	Fiji,
	New_Zealand,
	Micronesia,
	Kiribati,
	Marshall_Islands,
	Nauru,
	Palau,
	Papua_New_Guinea,
	Samoa,
	Solomon_Islands,
	Tonga,
	Tuvalu,
	Vanuatu,
	Timor_Leste,
	New_Caledonia,
	French_Polynesia,
	Cook_Islands,
	Wallis_and_Futuna,
	Niue,
	Tokelau,
	Northern_Mariana_Islands,
	Guam,
];
pub const NORTH_AMERICA: [Country; 38] = [
	Global,
	Anguilla,
	Antigua_and_Barbuda,
	Bahamas,
	Belize,
	Bermuda,
	Barbados,
	British_Virgin_Islands,
	Cayman_Islands,
	Canada,
	Costa_Rica,
	Cuba,
	Dominica,
	Dominican_Republic,
	El_Salvador,
	Grenada,
	Greenland,
	Guatemala,
	Guadeloupe,
	Haiti,
	Honduras,
	Jamaica,
	Martinique,
	Mexico,
	Montserrat,
	Nicaragua,
	Panama,
	Puerto_Rico,
	Saint_Barthelemy,
	Saint_Kitts_and_Nevis,
	Saint_Lucia,
	Sint_Maarten,
	Saint_Martin,
	Saint_Pierre_and_Miquelon,
	Saint_Vincent_and_the_Grenadines,
	Turks_and_Caicos_Islands,
	United_States,
	US_Virgin_Islands,
];
pub const SOUTH_AMERICA: [Country; 18] = [
	Global,
	Argentina,
	Bolivia,
	Brazil,
	Chile,
	Colombia,
	Ecuador,
	Falkland_Islands,
	French_Guiana,
	Guyana,
	Peru,
	Paraguay,
	Trinidad_and_Tobago,
	Uruguay,
	Suriname,
	Venezuela,
	Curacao,
	Aruba,
];

impl Country {
	/// Find in which [Continent] a [Country] is located
	/// # Panics
	/// Panics if the [Country] is not in any of the [continents][Continent], that shouldn't ever happen, so if it does report it on git repo!
	pub fn get_continent(&self) -> Continent {
		match self {
			c if EUROPE.contains(c) => Continent::Europe,
			c if AFRICA.contains(c) => Continent::Africa,
			c if ASIA.contains(c) => Continent::Asia,
			c if OCEANIA.contains(c) => Continent::Oceania,
			c if NORTH_AMERICA.contains(c) => Continent::North_America,
			c if SOUTH_AMERICA.contains(c) => Continent::South_America,
			_ => panic!("If this happened please report this on the git repo! (get_continent)"),
		}
	}

	/// Get a current [Country] by public endpoint IP Address
	pub fn find_current_country() -> Result<Self> {
		// Alternative GEO API = ifconfig.co/country
		let agent = ureq::AgentBuilder::new().user_agent(USER_AGENT).build();
		let response_str = match agent.get("https://get.geojs.io/v1/ip/country/full").call() {
			Ok(response) => response.into_string()?,
			Err(_) => {
				println!("First GEO API failed to respond!\nTrying backup one!");
				agent.get("http://ip-api.com/line/?fields=country").call()?.into_string()?
			}
		};
		let response_str = response_str.trim();

		Ok(match Self::from_str(response_str) {
			Ok(country) => country,
			Err(_) => Err(anyhow!(
				"Couldn't parse the country \"{}\" into the enum variant, please report this on the git repo!",
				response_str
			))?,
		})
	}
}

impl Continent {
	/// Get slice of all countries by [Continent]
	pub const fn get_countries(&self) -> &'static [Country] {
		match self {
			Continent::Europe => &EUROPE,
			Continent::Asia => &ASIA,
			Continent::Oceania => &OCEANIA,
			Continent::Africa => &AFRICA,
			Continent::North_America => &NORTH_AMERICA,
			Continent::South_America => &SOUTH_AMERICA,
		}
	}

	/// Get a current [Continent] by public endpoint IP Address
	pub fn find_current_continent() -> Result<Self> {
		let agent = ureq::AgentBuilder::new().user_agent(USER_AGENT).build();
		let response_str = agent.get("http://ip-api.com/line/?fields=continent").call()?.into_string()?;
		let response_str = response_str.trim();
		Ok(match Self::from_str(response_str) {
			Ok(continent) => continent,
			Err(_) => Err(anyhow!(
				"Couldn't parse the continent \"{}\" into the enum variant, please report this on the git repo!",
				response_str
			))?,
		})
	}
}
