#![warn(clippy::all, clippy::nursery)]
#![forbid(unsafe_code)]
#![feature(stmt_expr_attributes)]

use std::str::FromStr;

use colored::Colorize;
use strum::{Display, EnumString, EnumVariantNames};
use thiserror::Error;

pub mod geo;
pub mod mirror;

type Result<T> = std::result::Result<T, ShinyError>;

pub const BIN_NAME: &str = "shiny-mirrors";

pub const USER_AGENT: &str = "Shiny-Mirrors";

#[cfg(not(any(
	feature = "manjaro",
	feature = "arch",
	feature = "artix",
	feature = "rebornos",
	feature = "endeavouros"
)))]
compile_error!("At least one distro feature must be enabled!");

// #[cfg(multiple(feature = "manjaro", feature = "arch", feature = "artix", feature = "rebornos", feature = "endeavouros"))]
// compile_error!("You can't compile shiny-mirrors to support multiple distros at the same time! It must be compiled separately!");

pub trait MultiParse<T: FromStr> {
	#[inline]
	fn parse_multiple(str: &str) -> Option<Vec<T>> {
		let items: Vec<T> = str
			.split(',')
			.filter_map(|x| if x.is_empty() { None } else { T::from_str(x.trim()).ok() })
			.collect();

		if items.is_empty() {
			None
		} else {
			Some(items)
		}
	}
}

impl MultiParse<Self> for String {}

#[derive(Copy, Clone, Debug, Display, EnumVariantNames, EnumString)]
#[strum(serialize_all = "title_case")]
#[strum(ascii_case_insensitive)]
#[non_exhaustive]
pub enum FileSize {
	Big,
	Medium,
	Small,
}

#[derive(Copy, Clone, Debug, Display, EnumVariantNames, EnumString)]
#[strum(serialize_all = "title_case")]
#[strum(ascii_case_insensitive)]
pub enum MeasureMethod {
	Total,
	Transfer,
}

#[derive(Debug, Error)]
pub enum ShinyError {
	#[error("File that contains Local Mirror List is empty!\n{}", format ! ("Please run {} refresh", BIN_NAME).red())]
	LocalMirrorListFileEmpty,
	#[error("There seems to not be a local mirror list!\n{}", format ! ("Please run {} refresh", BIN_NAME).red())]
	NoLocalMirrorListFile,
	#[error("In local mirror list were not found any mirrors\n{}", format ! ("Please run {} refresh", BIN_NAME).red())]
	NoMirrorsInLocalMirrorList,
	#[error("Couldn't get a branch string from local mirror list!")]
	NoBranchInLocalMirrorList,
	#[error(
		"Couldn't find ANY mirrors for you! Maybe there were all filtered out because of the arguments you supplied or settings."
	)]
	NoMirrorsFound,
	#[error(transparent)]
	Other(#[from] anyhow::Error),
	#[error(transparent)]
	IoError(#[from] std::io::Error),
	#[error(transparent)]
	FtpError(#[from] suppaftp::FtpError),
	#[error(transparent)]
	SerdeError(#[from] serde_plain::Error),
	#[error(transparent)]
	UrlError(#[from] url::ParseError),
	#[error(transparent)]
	StrumError(#[from] strum::ParseError),
	#[error(transparent)]
	HttpError(#[from] ureq::Error),
	#[error("Unknown error")]
	Unknown,
}

impl From<()> for ShinyError {
	fn from(_: ()) -> Self {
		Self::Unknown
	}
}
