use std::{
	fs::{DirBuilder, OpenOptions},
	path::Path,
};

use clap::IntoApp;
use clap_generate::{
	generate,
	generators::{Bash, Elvish, Fish, Zsh},
};

use cli::Cli;

#[path = "src/cli.rs"]
mod cli;

fn main() {
	let out_dir = Path::new("../target/completions");

	if !out_dir.exists() {
		DirBuilder::new().recursive(true).create(&out_dir).unwrap();
	}

	let mut app = Cli::into_app();

	// FISH
	let mut fish_file = OpenOptions::new()
		.create(true)
		.write(true)
		.append(false)
		.open(out_dir.join("shiny-mirrors").with_extension("fish"))
		.unwrap();
	generate(Fish, &mut app, "shiny-mirrors", &mut fish_file);

	// ZSH
	let mut zsh_file = OpenOptions::new()
		.create(true)
		.write(true)
		.append(false)
		.open(out_dir.join("_shiny-mirrors"))
		.unwrap();
	generate(Zsh, &mut app, "shiny-mirrors", &mut zsh_file);

	// BASH
	let mut bash_file = OpenOptions::new()
		.create(true)
		.write(true)
		.append(false)
		.open(out_dir.join("shiny-mirrors").with_extension("bash"))
		.unwrap();
	generate(Bash, &mut app, "shiny-mirrors", &mut bash_file);

	// Elvish
	let mut elvish_file = OpenOptions::new()
		.create(true)
		.write(true)
		.append(false)
		.open(out_dir.join("shiny-mirrors").with_extension("elv"))
		.unwrap();
	generate(Elvish, &mut app, "shiny-mirrors", &mut elvish_file);
}
